package com.example.paginationtest.module.retrofit

import com.example.paginationtest.data.CommentBlModel
import com.example.paginationtest.module.retrofit.responses.CommentApiResponse

fun List<CommentApiResponse>.mapToCommentBlList(): List<CommentBlModel> =
    this.mapTo(ArrayList()) { commentApiModel -> commentApiModel.mapToCommentBl() }

fun CommentApiResponse.mapToCommentBl(): CommentBlModel =
    CommentBlModel(this.id, this.postId, this.name ?: "", this.email ?: "", this.body ?: "")