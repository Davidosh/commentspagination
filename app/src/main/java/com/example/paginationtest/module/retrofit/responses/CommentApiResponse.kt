package com.example.paginationtest.module.retrofit.responses

data class CommentApiResponse(
    val id: Long,
    val postId: Long,
    val name: String? = null,
    val email: String? = null,
    val body: String? = null
)