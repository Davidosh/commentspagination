package com.example.paginationtest.module.retrofit

import com.example.paginationtest.data.CommentBlModel
import com.example.paginationtest.module.interfaces.ApiModule
import javax.inject.Inject

class RetrofitModuleImpl @Inject constructor(
    retrofit: RetrofitProvider
) : ApiModule {

    private var api: MainApi = retrofit.buildRetrofit().create(MainApi::class.java)

    override suspend fun getComments(startPosition: Long, loadSize: Int): List<CommentBlModel> {
        return api.getComments(startPosition, loadSize).mapToCommentBlList()
    }
}