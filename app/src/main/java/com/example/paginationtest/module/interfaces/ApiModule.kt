package com.example.paginationtest.module.interfaces

import com.example.paginationtest.data.CommentBlModel
import retrofit2.http.Query

interface ApiModule {
    suspend fun getComments(
        @Query("start") startPosition: Long,
        @Query("limit") loadSize: Int
    ): List<CommentBlModel>
}