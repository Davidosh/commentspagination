package com.example.paginationtest.module.retrofit

import com.example.paginationtest.module.retrofit.responses.CommentApiResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface MainApi {

    @GET("/comments")
    suspend fun getComments(
        @Query("_start") startPosition: Long,
        @Query("_limit") loadSize: Int
    ): List<CommentApiResponse>

}