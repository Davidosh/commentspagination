package com.example.paginationtest.di.component

import android.content.Context
import com.example.paginationtest.di.modules.ActivityDIModule
import com.example.paginationtest.di.modules.ModulesDIModule
import com.example.paginationtest.di.modules.ApplicationModule
import com.example.paginationtest.PaginationTestApp
import com.example.paginationtest.di.modules.RepositoryDIModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


/**
 * Main component for the application.
 *
 */
@Singleton
@Component(
    modules = [
        ApplicationModule::class,
        ModulesDIModule::class,
        RepositoryDIModule::class,
        ActivityDIModule::class,
        AndroidSupportInjectionModule::class
    ]
)
interface ApplicationComponent : AndroidInjector<PaginationTestApp> {

    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): ApplicationComponent
    }
}
