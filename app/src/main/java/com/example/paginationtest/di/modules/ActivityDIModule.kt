package com.example.paginationtest.di.modules

import com.example.paginationtest.di.ui.ViewModelBuilder
import com.example.paginationtest.ui.comments.CommentsFragment
import com.example.paginationtest.ui.main.MainActivity
import com.example.paginationtest.ui.splash.SplashFragment
import com.example.paginationtest.ui.welcome.WelcomeFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityDIModule {

  //Add activities and fragments here

    @ContributesAndroidInjector(modules = [ViewModelBuilder::class, ViewModelModule::class])
    internal abstract fun mainActivityInjector(): MainActivity

    @ContributesAndroidInjector(modules = [ViewModelBuilder::class, ViewModelModule::class])
    internal abstract fun splashFragmentInjector(): SplashFragment

    @ContributesAndroidInjector(modules = [ViewModelBuilder::class, ViewModelModule::class])
    internal abstract fun welcomeFragmentInjector(): WelcomeFragment

    @ContributesAndroidInjector(modules = [ViewModelBuilder::class, ViewModelModule::class])
    internal abstract fun commentsFragmentInjector(): CommentsFragment


}