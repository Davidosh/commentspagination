package com.example.paginationtest.di.modules

import androidx.lifecycle.ViewModel
import com.example.paginationtest.di.ui.ViewModelKey
import com.example.paginationtest.ui.comments.CommentsViewModel
import com.example.paginationtest.ui.splash.SplashViewModel
import com.example.paginationtest.ui.welcome.WelcomeViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    //Add viewModels here

    @Binds
    @IntoMap
    @ViewModelKey(SplashViewModel::class)
    abstract fun bindSplashViewModel(viewModel: SplashViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(WelcomeViewModel::class)
    abstract fun bindWelcomeViewModel(viewModel: WelcomeViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CommentsViewModel::class)
    abstract fun bindCommentsViewModel(viewModel: CommentsViewModel): ViewModel
}