package com.example.paginationtest.di.modules

import com.example.paginationtest.repository.comments.CommentsRepositoryImpl
import com.example.paginationtest.repository.interfaces.CommentsRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryDIModule {

    //todo add repositories here

    @Provides
    @Singleton
    fun commentsRepositoryProvide(commentsRepositoryImpl: CommentsRepositoryImpl) = commentsRepositoryImpl as CommentsRepository
}