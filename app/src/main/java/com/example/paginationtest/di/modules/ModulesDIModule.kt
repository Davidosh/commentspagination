package com.example.paginationtest.di.modules


import com.example.paginationtest.module.retrofit.RetrofitModuleImpl
import com.example.paginationtest.module.retrofit.RetrofitProvider
import com.example.paginationtest.module.interfaces.ApiModule
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ModulesDIModule {

    //todo add modules here

    @Provides
    @Singleton
    fun apiModuleProvide(retrofitModule: RetrofitModuleImpl) = retrofitModule as ApiModule

    @Provides
    @Singleton
    fun retrofitProvide() = RetrofitProvider().buildRetrofit()
}