package com.example.paginationtest.ui.comments

import android.content.Context
import androidx.lifecycle.MutableLiveData
import com.example.paginationtest.data.CommentBlModel
import com.example.paginationtest.repository.interfaces.CommentsRepository
import com.example.paginationtest.ui.base.BaseViewModel
import javax.inject.Inject

class CommentsViewModel @Inject constructor(
    context: Context,
    private val commentsRepository: CommentsRepository
) : BaseViewModel(context) {

    val commentsLiveData = MutableLiveData<List<CommentBlModel>>()


    fun onInit(lowerBound: Long, upperBound: Long) {
        if (commentsLiveData.value.isNullOrEmpty()) {
            launchWithProgress {
                val commentsList = commentsRepository.getFirstPaginationPage(lowerBound, upperBound)
                commentsLiveData.postValue(commentsList)
            }
        }
    }

    fun onCommentsScrolled(lastVisibleItemPosition: Int) {
        val needToLoadMoreComments =
            commentsLiveData.value != null && lastVisibleItemPosition == commentsLiveData.value!!.size - 1
        val isLoading = progress.value != null && progress.value!!
        if (commentsRepository.hasMoreComments && !isLoading && needToLoadMoreComments) {
            progress.value = true
            loadMoreComments()
        }
    }

    private fun loadMoreComments() {
        launch {
            val commentsList = commentsRepository.getCommentsNextPaginationPage()
            val oldCommentsList = commentsLiveData.value ?: mutableListOf()
            commentsLiveData.postValue(oldCommentsList + commentsList)
            progress.postValue(false)
        }
    }
}