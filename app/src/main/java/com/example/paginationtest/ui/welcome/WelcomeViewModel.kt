package com.example.paginationtest.ui.welcome

import android.content.Context
import com.example.paginationtest.repository.interfaces.CommentsRepository
import com.example.paginationtest.ui.base.BaseViewModel
import com.example.paginationtest.ui.welcome.model.CommentsBoundsUIModel
import com.example.paginationtest.utils.live_data.event
import com.example.paginationtest.utils.live_data.setEvent
import kotlinx.coroutines.Job
import javax.inject.Inject

class WelcomeViewModel @Inject constructor(
    context: Context,
    private val commentsRepository: CommentsRepository
) : BaseViewModel(context) {

    val goToCommentsScreenEvent = event<CommentsBoundsUIModel>()

    var lastFetchCommentJob: Job? = null

    fun onGetCommentsClicked(lowerBound: String?, upperBound: String?) {
        if (!isValidInputFields(lowerBound, upperBound)) {
            return
        }
        goToCommentsScreenEvent.setEvent(
            CommentsBoundsUIModel(
                lowerBound!!.toLong(),
                upperBound!!.toLong()
            )
        )
    }

    private fun isValidInputFields(lowerBound: String?, upperBound: String?): Boolean {
        when {
            lowerBound.isNullOrBlank() -> {
                errors.setEvent(Throwable("Lower bound is empty"))
                return false
            }
            upperBound.isNullOrBlank() -> {
                errors.setEvent(Throwable("Upper bound is empty"))
                return false
            }
            lowerBound.toLongOrNull() == null -> {
                errors.setEvent(Throwable("Lower bound should be number"))
                return false
            }
            lowerBound.toLong() < 1 -> {
                errors.setEvent(Throwable("Lower bound should be positive number"))
                return false
            }
            upperBound.toLongOrNull() == null -> {
                errors.setEvent(Throwable("Upper bound should be number"))
                return false
            }
            upperBound.toLong() < 1 -> {
                errors.setEvent(Throwable("Upper bound should be positive number"))
                return false
            }
            upperBound.toLong() < lowerBound.toLong() -> {
                errors.setEvent(Throwable("Lower bound should be grater than upper bound"))
                return false
            }
            else -> {
                return true
            }
        }
    }

}