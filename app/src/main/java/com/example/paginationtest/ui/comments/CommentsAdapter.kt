package com.example.paginationtest.ui.comments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.paginationtest.R
import com.example.paginationtest.data.CommentBlModel
import kotlinx.android.synthetic.main.item_comment_holder.view.*

enum class ViewHolderType {
    COMMENT, LOADING
}

class CommentsAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    var data = listOf<CommentsUiModel>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == ViewHolderType.COMMENT.ordinal) {
            val cellCommentView =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_comment_holder, parent, false)
            CommentViewHolder(cellCommentView)
        } else {
            val cellLoadingView =
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.item_loading_holder, parent, false)
            LoadingViewHolder(cellLoadingView)
        }

    }

    override fun getItemViewType(position: Int): Int {
        return if (data[position].isLoadingItem) {
            ViewHolderType.LOADING.ordinal
        } else {
            ViewHolderType.COMMENT.ordinal
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is CommentViewHolder) {
            holder.bind(data[position].comment)
        }
    }

    class CommentViewHolder(views: View) : RecyclerView.ViewHolder(views) {

        private var model: CommentBlModel? = null

        fun bind(model: CommentBlModel?) {
            this.model = model
            if(model == null){
                return
            }
            itemView.textViewId.text = itemView.context.getString(R.string.comment_text_id, model.id.toString())
            itemView.textViewName.text = itemView.context.getString(R.string.comment_text_name, model.name)
            itemView.textViewEmail.text = itemView.context.getString(R.string.comment_text_email, model.email)
            itemView.textViewBody.text = itemView.context.getString(R.string.comment_text_body, model.body)
        }

    }

    class LoadingViewHolder(views: View) : RecyclerView.ViewHolder(views) {
        //todo
    }
}