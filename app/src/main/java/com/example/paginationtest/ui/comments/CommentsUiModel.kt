package com.example.paginationtest.ui.comments

import com.example.paginationtest.data.CommentBlModel

data class CommentsUiModel(
    val isLoadingItem: Boolean = false,
    val comment: CommentBlModel? = null
)