package com.example.paginationtest.ui.base

import android.content.Context
import android.os.Handler
import android.os.Looper.getMainLooper
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.paginationtest.utils.live_data.event
import com.example.paginationtest.utils.live_data.postEvent
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO

abstract class BaseViewModel(private val context: Context) : ViewModel() {

    protected val TAG = javaClass.simpleName
    val errors = event<Throwable>()
    val progress = MutableLiveData<Boolean>()
    private val coroutineErrorHandler = CoroutineExceptionHandler { context, exception ->
        errors.postEvent(exception)
        progress.postValue(false)
        exception.printStackTrace()

        Handler(getMainLooper()).post {
            Toast.makeText(this.context, exception.message, LENGTH_LONG).show()
        }
    }

    fun launch(
        block: suspend () -> Unit
    ): Job {
        return viewModelScope.launch(IO + coroutineErrorHandler) {
            try {
                block()
            } catch (e: Exception) {
                throw e
            }
        }
    }

    fun launchWithProgress(
        block: suspend () -> Unit
    ): Job {
        return viewModelScope.launch(IO + coroutineErrorHandler) {
            withContext(Dispatchers.Main) { progress.value = true }
            try {
                block()
            } catch (e: Exception) {
                throw e
            } finally {
                withContext(Dispatchers.Main) { progress.value = false }
            }
        }
    }
}