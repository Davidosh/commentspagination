package com.example.paginationtest.ui.splash

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.paginationtest.R
import com.example.paginationtest.ui.base.BaseFragment
import com.example.paginationtest.utils.live_data.observeEvent

class SplashFragment : BaseFragment(R.layout.fragment_splash) {

    private val viewModel by viewModels<SplashViewModel> { viewModelFactory }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.onViewCreated()
    }

    override fun initClickListeners() {
        //todo
    }

    override fun initObservers() {
        viewModel.goToWelcomeScreenEvent.observeEvent(viewLifecycleOwner) {
            findNavController().navigate(R.id.action_splashScreenFragment_to_welcomeFragment)
        }
        viewModel.errors.observeEvent(viewLifecycleOwner) { throwable ->
            showError(throwable)
        }
    }
}