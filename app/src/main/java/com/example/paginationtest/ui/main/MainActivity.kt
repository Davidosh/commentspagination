package com.example.paginationtest.ui.main

import android.os.Bundle
import com.example.paginationtest.R
import com.example.paginationtest.ui.base.BaseActivity

class MainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}