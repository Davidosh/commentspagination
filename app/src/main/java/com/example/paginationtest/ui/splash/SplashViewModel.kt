package com.example.paginationtest.ui.splash

import android.content.Context
import androidx.lifecycle.viewModelScope
import com.example.paginationtest.ui.base.BaseViewModel
import com.example.paginationtest.utils.live_data.event
import com.example.paginationtest.utils.live_data.postEvent
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

class SplashViewModel @Inject constructor(
    context: Context
) : BaseViewModel(context) {

    val goToWelcomeScreenEvent =
        event<Unit>()

    fun onViewCreated() {
        viewModelScope.launch {
            delay(3000L)
            goToWelcomeScreenEvent.postEvent(Unit)
        }
    }
}