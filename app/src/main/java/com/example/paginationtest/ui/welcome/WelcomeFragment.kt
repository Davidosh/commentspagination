package com.example.paginationtest.ui.welcome

import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.paginationtest.R
import com.example.paginationtest.ui.base.BaseFragment
import com.example.paginationtest.utils.live_data.observeEvent
import kotlinx.android.synthetic.main.fragment_welcome.*

class WelcomeFragment : BaseFragment(R.layout.fragment_welcome) {

    private val viewModel by viewModels<WelcomeViewModel> { viewModelFactory }

    override fun initClickListeners() {
        buttonGetComments.setOnClickListener {
            viewModel.onGetCommentsClicked(
                editTextLowerBound.text.toString(),
                editTextUpperBound.text.toString()
            )
        }
    }

    override fun initObservers() {
        viewModel.goToCommentsScreenEvent.observeEvent(viewLifecycleOwner) {
            findNavController().navigate(
                WelcomeFragmentDirections.actionWelcomeFragmentToCommentsFragment(
                    it.lowerBound,
                    it.upperBound
                )
            )
        }
        viewModel.errors.observeEvent(viewLifecycleOwner) { throwable ->
            showError(throwable)
        }
        viewModel.progress.observe(viewLifecycleOwner, Observer { isProgress ->
            frameLayoutProgress.isVisible = isProgress
            editTextLowerBound.isEnabled = !isProgress
            editTextUpperBound.isEnabled = !isProgress
            buttonGetComments.isEnabled = !isProgress
        })
    }
}