package com.example.paginationtest.ui.comments

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.paginationtest.R
import com.example.paginationtest.ui.base.BaseFragment
import com.example.paginationtest.utils.live_data.observeEvent
import kotlinx.android.synthetic.main.fragment_comments.*

class CommentsFragment : BaseFragment(R.layout.fragment_comments) {

    private val viewModel by viewModels<CommentsViewModel> { viewModelFactory }
    private var adapter: CommentsAdapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private val args: CommentsFragmentArgs by navArgs()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initAdapter()
        viewModel.onInit(args.lowerBound, args.upperBound)
        super.onViewCreated(view, savedInstanceState)
    }

    override fun initClickListeners() {

    }

    override fun initObservers() {
        viewModel.commentsLiveData.observe(viewLifecycleOwner, Observer { commentsList ->
            adapter!!.data = commentsList.map {
                CommentsUiModel(comment = it)
            }
            textViewNoItemsFound.isVisible = commentsList.isNullOrEmpty()
            recyclerViewComments.isVisible = !commentsList.isNullOrEmpty()

        })
        viewModel.errors.observeEvent(viewLifecycleOwner) { throwable ->
            showError(throwable)
        }
        viewModel.progress.observe(viewLifecycleOwner, Observer { isProgress ->
            val oldList = adapter?.data ?: mutableListOf()
            val newList = oldList.toMutableList()
            if (isProgress) {
                if (newList.isEmpty() || !newList.last().isLoadingItem) {
                    newList.add(CommentsUiModel(isLoadingItem = true))
                    adapter?.data = newList
                }
            } else {
                if (!newList.isNullOrEmpty() && newList.last().isLoadingItem) {
                    newList.remove(newList.last())
                    adapter?.data = newList
                }
            }
        })
    }

    private fun initAdapter() {
        adapter = CommentsAdapter()
        layoutManager = LinearLayoutManager(requireContext(), RecyclerView.VERTICAL, false)
        recyclerViewComments.layoutManager = layoutManager
        recyclerViewComments.adapter = adapter
        recyclerViewComments.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                viewModel.onCommentsScrolled(layoutManager!!.findLastVisibleItemPosition())
            }
        })
    }

}