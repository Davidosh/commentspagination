package com.example.paginationtest.ui.welcome.model

data class CommentsBoundsUIModel(val lowerBound: Long, val upperBound: Long)