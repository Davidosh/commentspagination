package com.example.paginationtest.repository.comments

import com.example.paginationtest.data.CommentBlModel
import com.example.paginationtest.module.interfaces.ApiModule
import com.example.paginationtest.repository.interfaces.CommentsRepository
import javax.inject.Inject

class CommentsRepositoryImpl @Inject constructor(
    private val apiModule: ApiModule
) : CommentsRepository {

    override val hasMoreComments: Boolean
        get() = hasMoreCommentsToLoad

    override val paginationSize: Int
        get() = 10

    private var hasMoreCommentsToLoad = true
    private var lastPositionInPagination = 0L
    private var lowerBoundForComments = 0L
    private var upperBoundForComments = 0L


    override suspend fun getFirstPaginationPage(lowerBound: Long, upperBound: Long): List<CommentBlModel> {
        resetPagination()
        lowerBoundForComments = lowerBound
        upperBoundForComments = upperBound
        lastPositionInPagination = lowerBound
        if (upperBoundForComments - lowerBoundForComments < paginationSize) {
            val commentsPortion = apiModule.getComments(
                lowerBoundForComments - 1,
                (upperBoundForComments - lowerBoundForComments).toInt() + 1
            )
            // commentsList.addAll(commentsPortion)
            lastPositionInPagination = upperBoundForComments
            hasMoreCommentsToLoad = false
            // commentsList
            return commentsPortion
        } else {
            val commentsPortion =
                apiModule.getComments(lastPositionInPagination - 1, paginationSize)
            //commentsList.addAll(commentsPortion)
            lastPositionInPagination += paginationSize
            //commentsList
            return commentsPortion
        }
    }

    override suspend fun getCommentsNextPaginationPage(): List<CommentBlModel> {
        if (!hasMoreCommentsToLoad) {
            return mutableListOf()
        } else if (upperBoundForComments - lastPositionInPagination < paginationSize) {
            val commentsPortion = apiModule.getComments(
                lastPositionInPagination - 1,
                (upperBoundForComments - lastPositionInPagination).toInt() + 1
            )
            //commentsList.addAll(commentsPortion)
            lastPositionInPagination = upperBoundForComments
            hasMoreCommentsToLoad = false
            return commentsPortion
            //commentsList
        } else {
            val commentsPortion =
                apiModule.getComments(lastPositionInPagination - 1, paginationSize)
            //commentsList.addAll(commentsPortion)
            lastPositionInPagination += paginationSize
            //commentsList
            return commentsPortion
        }
    }

    private fun resetPagination() {
        lastPositionInPagination = -1L
        lowerBoundForComments = -1L
        upperBoundForComments = -1L
        hasMoreCommentsToLoad = true
    }

}