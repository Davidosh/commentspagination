package com.example.paginationtest.repository.interfaces

import com.example.paginationtest.data.CommentBlModel

interface CommentsRepository {

    val hasMoreComments: Boolean

    val paginationSize: Int

    suspend fun getFirstPaginationPage(lowerBound: Long, upperBound: Long): List<CommentBlModel>

    suspend fun getCommentsNextPaginationPage(): List<CommentBlModel>

}