package com.example.paginationtest.data

data class CommentBlModel(
    val id: Long,
    val postId: Long,
    val name: String,
    val email: String,
    val body: String
)