package com.example.paginationtest.utils.live_data

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData

fun <T> MutableLiveData<Event<T>>.observeEvent(owner: LifecycleOwner, block: (T) -> Unit) {
    observe({ owner.lifecycle }) { event ->
        event?.getContentIfNotHandled()?.let(block)
    }
}

fun <T> MutableLiveData<T>.observe(owner: LifecycleOwner, block: (T) -> Unit) {
    observe({ owner.lifecycle }) { block.invoke(it) }
}

fun <T> MutableLiveData<Event<T>>.setEvent(item: T) {
    value = Event(item)
}

fun <T> MutableLiveData<Event<T>>.postEvent(item: T) {
    postValue(Event(item))
}

fun <T> event(defValue: T? = null) = MutableLiveData<Event<T>>().apply {
    defValue?.let {
        value =
            Event(it)
    }
}

fun <T> liveData(defValue: T? = null) = MutableLiveData<T>().apply {
    defValue?.let { value = it }
}